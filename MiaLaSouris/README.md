Mia la sourie
===========

Développé par <Jeyda Parlar> <Louca Desombre>
Contacts : <jeyda.parlar.etu@univ-lille.fr> , <louca.desombre.etu@univ-lille.fr>

# Présentation de <Mia la Souris>

<Description de votre jeu>
Le jeu suis l'histoire d'une souris se nommant Mia qui a perdue son chemin. Le joueur doit donc reléver des défis tel que résoudre un labyrinthe et répondre a des questions à choix multiples. 
Des captures d'écran illustrant le fonctionnement du logiciel sont proposées dans le répertoire shots.


# Utilisation de <le Mia la souris>

Afin d'utiliser le projet, il suffit de taper les commandes suivantes dans un terminal :

```
./compile.sh
```
Permet la compilation des fichiers présents dans 'src' et création des fichiers '.class' dans 'classes'

```
./run.sh
```
Permet le lancement du jeu
