import java.rmi.server.SocketSecurityException;

import extensions.CSVFile;
class Mialasourie extends Program {
    
    // utlisation de plus de types/class comme un type joueur pour tout stocker dans un fichier au lieu d'en crée un par utilisateur
    // fichier joueur en csv
    final char NEW_LINE = '\n';


    // Variable du Joueur
    String Prenom = "";
    String Nom = "";
    String Animale = "";
    String Level = "1";
    String Score = "0";
    int pts = 0;


    // Sert a cree un nouveux joueur 
    Joueur newJoueur(String Prenom,String Nom,String Animale, String Level, String Score){
        Joueur j= new Joueur();
        j.Prenom= Prenom;
        j.Nom=Nom;
        j.Animale=Animale;
        j.Level=Level;
        j.Score=Score;
        return j;
    }


    // Sert a rechercher une valeur dans un .txt
    String rechercherValeur(String chaine, String cle) {
        String valeur = "";
        int indice = 0;
        while (indice < length(chaine) && indice + length(cle) < length(chaine) && 
               !equals(cle, substring(chaine, indice, indice+length(cle)))) {
            indice = indice + 1;
        }
        if (indice < length(chaine) - length(cle)) {
            int indiceRetourLigne = indice;
            while (indiceRetourLigne < length(chaine) && charAt(chaine, indiceRetourLigne) != NEW_LINE) {
                indiceRetourLigne = indiceRetourLigne + 1;
            }
            valeur = substring(chaine, indice+length(cle), indiceRetourLigne);
        }
        return valeur;
    }

    // sert a enregistré 
    void enregistrer(String nomfichier, Joueur j){
        CSVFile fichier = loadCSV(nomfichier, ',');
        int col = columnCount(fichier);
        int li = rowCount(fichier);
        String [][] tab= new String [li+1][col];
        for (int l = 0; l < li; l = l+1){
            for (int c = 0; c < col; c = c+1){
                tab[l][c]= getCell(fichier,l, c);
        }
        }
        int l = li;
        int c = 0;
            tab[l][c]= j.Prenom;
            c++;
            tab[l][c]= j.Nom;
             c++;
            tab[l][c]= j.Animale;
             c++;
            tab[l][c]= j.Level;
             c++;
            tab[l][c]= j.Score;
        saveCSV(tab,nomfichier);
    }


    // Crée boite automatiquement
    String ligne(String Phrase){
        String finale = "";
        int taille = length(Phrase) + 8;

        for (int cpt = 0; cpt < taille; cpt = cpt +1) {
            finale = finale + "#";
        }

        finale = finale + NEW_LINE;
        return finale;
    }

    // Crée boite auyomatiquement
    String cote(String Phrase, int nb){
        int taille = length(Phrase) + 8;
        String finale = "";
        int idx = 0;
        int nbLigne = nb / 2 - 1; // le -1 sert a ne pas compter la premier et la dernier lignes 

        while(idx < nbLigne) { 
            idx = idx + 1;
            finale = finale + "#";
            for (int cpt = 0; cpt < taille-2; cpt = cpt +1) {
                finale = finale + " ";
            }
            finale = finale + "#";
            finale = finale + NEW_LINE;
        }
        return finale;
    }

    // Crée boite auyomatiquement
    String phrase(String Phrase){
        int espace = 3;
        String finale = "";

        finale = finale + "#";
        for (int cpt = 0; cpt < espace; cpt = cpt +1) {
                finale = finale + " ";
            }
        finale = finale + Phrase;
        for (int cpt = 0; cpt < espace; cpt = cpt +1) {
                finale = finale + " ";
            }
        finale = finale + "#";
        finale = finale + NEW_LINE;
        return finale;
    }

    // Crée boite auyomatiquement
    String menu(String Phrase, int nb) {
        String finale = "";
        int idx = 0;

        finale = finale + ligne(Phrase);
        finale = finale + cote(Phrase, nb);
        finale = finale + phrase(Phrase);
        finale = finale + cote(Phrase, nb);
        finale = finale + ligne(Phrase);

        return finale;
    }
// fin cree boite automatiqument 

    void testMenu() {
       assertEquals("##########" + NEW_LINE + "#   cc   #" + NEW_LINE + "##########" + NEW_LINE, menu("cc", 3));
    }

    // sert a initialiser les variables 
    void menuVariable() {
        final String CONTENU = fileAsString("ressources/menu/variable.txt");
        hide();

        clearScreen();
        print(menu(rechercherValeur(CONTENU, "1 : "), 5));
        Prenom = readString();

        clearScreen();
        print(menu(rechercherValeur(CONTENU, "2 : "), 5));
        Nom = readString();

        Animale = "Souris";
        Level = "1";
        Joueur joueur=newJoueur(Prenom,Nom,Animale,Level,Score);
        enregistrer("src/save/Joueur.csv",joueur);
    }

    //Creation de la souris pour intro 
    String Souris () {
        final String SOURIS= fileAsString("ressources/character/souris.txt");
        int cpt = 1; 
        String souris = "";
        while(cpt<6){
            souris=souris+rechercherValeur(SOURIS,  cpt + " : ")+ NEW_LINE;
            cpt=cpt+1;
        }
        return(souris);
    }


    // Introduction a l'hitoire
    void introduction(){
        int cpt = 1; 
        char suite = 's';
        final String CONTENU = fileAsString("ressources/level/level1/Histoire1.txt");
        clearScreen();

        while(cpt < 8){
            String q = rechercherValeur(CONTENU, cpt + " : ");
            println(Souris());
            println(menu(q, 5));
            println("(Taper \"s\" puis sur entrée pour la suite)");
            char car = readChar();

            if (suite == car ){
                cpt = cpt+1;
                clearScreen();
            }else{
                car = readChar();
            }
        }
            
    }


    // Start post intro
    void Debut(){
        char suite = 's';
        do {
            clearScreen();
            println("Votre Score "+Score+"pts");
            println(menu("Début du jeu...!",5));
            println("(Taper \"s\" puis sur entrée pour la suite)");
        } while (readChar() != suite);
    }


    // CSV transfo en tab 2
    String[][] CSVtoTab(String nomfichier){
        CSVFile fichier = loadCSV(nomfichier, ',');
        int col= columnCount(fichier);
        int li= rowCount(fichier);
        String [][] tab= new String [li][col];

        for (int i = 0; i < length(tab,1); i = i+1){
            for(int j=  0; j < length(tab,2); j = j+1){
                tab[i][j] = getCell(fichier,i,j);
            }
        }
        return tab;
    }


    // level Question
    void level(int nblevel, String nbScore){
        String [][] questions = CSVtoTab("ressources/level/level" + nblevel + "/level" + nblevel + ".csv");
        int nb=length(questions,1);
        int i=1;
        while(i<nb){
            clearScreen();
            println("Votre Score : "+ stringToScore(Score) +"pts");
            for (int cpt = 1; cpt < length(questions,2) - 1; cpt = cpt +1) {
                println(questions[i][cpt]);
            }
            println("Saisissez la lettre correspondant à votre réponse, puis appuyez sur entrée!");
            String réponse = questions[i][5];
            String saisie = readString();

            if (equals(saisie,réponse)){
                println("Bonne réponse! ");
                Score = Score + nbScore;
            }else if (!equals(saisie,réponse)){
                println("Mauvaise réponse! ");
            }

            println(NEW_LINE);
            println("(Taper \"s\" puis sur entrée pour la suite)");
            if (readChar() == 's') {
                i = i+1;
            }

        }
            
    }

    int stringToScore(String Score) {
        return length(Score) -1 ;
    }

    void testStringToScore() {
        assertEquals(2, stringToScore("@@@"));
        assertEquals(5, stringToScore("@@@@@@"));
        assertEquals(10, stringToScore("@@@@@@@@@@@"));
    }

    // Afiche tab du lab
    void afficher(String [][] lab) {
        for (int l = 0; length(lab, 1) > l; l = l+1) {
            for (int c = 0; length(lab, 2) > c; c = c +1 ) {
               print(lab [l][c]);
            }
            println();
        }
    }


    // Place le joueur dans le tab
    void afficherJoueur(String [][] lab, int c, int l, int ol, int oc) {
        String joueur = "*";
        lab [l][c] = joueur;
        lab [ol][oc] = " ";
    }


    // Sert a ne pas sortir des mur
    boolean mur(String lab [][], int l, int c) {
        boolean mur = false;
        if(equals(lab [l][c], "/")) {
            mur = true;
        }
        return mur;
    }


    // Sert a savoir quand on parle a qq
    boolean fin(String lab [][], int l, int c) {
        boolean fin = false;
        if(equals(lab [l][c], "O")) {
            fin = true;
        }
        return fin;
    }


    // Gere tout les lab
    void lab(int nblevel, int lStart, int cStart){
        int l = lStart;
        int c = cStart;
        int ol = l;
        int oc = c;
        String [][] lab = CSVtoTab("ressources/level/lab/lab" + nblevel + ".csv");
        char déplacement = ' ';
        String joueur = "*";
        boolean fin = fin(lab, l, c);
        lab [l][c] = joueur;
        clearScreen();
        afficher(lab);
        while(fin == false) {
            do {
                déplacement = readChar();
            }while (déplacement != 'z' && déplacement != 'q' && déplacement != 's' && déplacement != 'd');
            if(déplacement == 'z' && mur(lab, l - 1, c) == false) {
                l = l - 1;
            } 
            if (déplacement == 's' && mur(lab, l + 1 , c) == false) {
                l = l + 1;
            }
            if (déplacement == 'q' && mur(lab, l, c - 1) == false) {
                c = c - 1;
            }
            if (déplacement == 'd' && mur(lab, l, c + 1) == false) {
                c = c + 1;
            }
            fin = fin(lab, l, c);
            clearScreen();
            afficherJoueur(lab, c, l, ol, oc);
            afficher(lab);
            ol = l;
            oc = c;
        }
        clearScreen();
        print(menu("Bravo, continuez comme sa !!", 5));
        println(NEW_LINE);
        do {
            println("(Taper \"s\" puis sur entrée pour la suite)");
        } while (readChar() != 's'); 
    }


    //Fin de jeux  
        void end () {
            clearScreen();
            println(Souris());
            println(menu("Bravo, tu a fini le jeu",5));
            println(NEW_LINE);
            println("Voila ton score : "+ stringToScore(Score) +"pts");
        }


    void algorithm(){
        menuVariable();
        introduction();
        Debut();
        lab(1, 6, 3);
        level(1, "@");
        lab(2, 9, 2);
        level(2, "@@");
        lab(3, 9, 2);
        level(3, "@@@");
        end();
    }
}

